var express = require('express');
var app = express();
var bodyParser = require('body-parser');
//var mongoose   = require('mongoose');

console.log(process.env.NODE_ENV);

//mongoose.connect('mongodb://db:27017/app');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


var port = 3001; //process.env.PORT 

var currentAlarmsRouter = require('./app/routes/currentAlarms.js');
var historyAlarmsRouter = require('./app/routes/historyAlarms.js')

app.use('/api', currentAlarmsRouter);
app.use('/api', historyAlarmsRouter);





//socket.io config


// setting up socket.io



var server = require('http').Server(app);
var io = require('socket.io')(server);




io.on('connection', function (socket) {
  console.log("Connection to socket");
  socket.on('dispatch_action', function(data,fn){
      socket.broadcast.emit('dispatch_action', data);
      console.log(`Disptached action: ${data.action} ${data.payload.id} ${data.payload.seq}`)
      fn('ok')
  });
  socket.on('heart_beat', function(data,fn){
      socket.broadcast.emit('heart_beat', data);
      console.log("emit heart_beat");
      fn('ok');
  });

});


//server = app.listen(port);

server.listen(port);


console.log("Listen on ...");

module.exports = server
