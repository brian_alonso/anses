const express = require('express');
const fs = require('fs')
const getHistoryAlarms = require('/app/retriever/getHistoryAlarms')
var router = express.Router();

router.use(function(req, res, next){
  console.log("Algo esta pasando en la api route");
  next();
});



router.route('/historyAlarms')
    
    .get(function(req,res){
            getHistoryAlarms.getAlarms()
            .then((alarms)=>{
              res.json(alarms)
            })
            .catch((err)=>{
              res.json(err)
            })
    });
    
    

module.exports = router;
