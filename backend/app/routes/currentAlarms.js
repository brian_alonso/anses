const express = require('express');
const fs = require('fs')

var router = express.Router();

router.use(function(req, res, next){
  console.log("Algo esta pasando en la api route");
  next();
});



router.route('/currentAlarms')
    
    .get(function(req,res){
            fs.readFile("/shared/currentAlarms.json", 'utf8', function (err, alarms) {
                if (err){
                  res.json({error: err})
                }
                res.json(JSON.parse(alarms));
            })
    });



module.exports = router;
