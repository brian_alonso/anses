let mongoose = require("mongoose");
let Client = require('../app/models/client');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
let newClient = { cliente: {
        serv: 1,
        cliente: 'brian',
        equipo: 'boca',
        sitio: 'san justo',
        ip: '127.0.0.1',
    
}
}



chai.use(chaiHttp);

describe('Clients', () => {
    beforeEach((done) => {
        Client.remove({}, (err) =>{
            done();
        });
    });


    describe('/GET client', () => {
        it('it should GET all the clients', (done) => {
            chai.request(server)
                .get('/api/clients')
                .end((err,res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                done();
            }); 
            
        });
    });

    describe('/POST client', () => {
        it('it should not POST a client without cliente field', (done) => {
            let client = {
                author: "jorge"
            }
            chai.request(server)
                .post('/api/clients')
                .send(client)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('cliente');
                    res.body.errors.cliente.should.have.property('kind').eql('required');
                    done();
                });
        });
        it('it should POST a client', (done) => {
           let client = newClient
           chai.request(server)
                .post('/api/clients')
                .send(client)
                .end((err, res) => {
                    
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Client successfully added!');
                    res.body.client.should.have.property('cliente');
                    res.body.client.should.have.property('cliente').eql('brian');
                    done();
                });
            
        });
    });
    
    
    describe('/GET/:id client', () => {
        it('it should GET a client by the given id', (done) =>{
            let client = new Client(newClient.cliente);
            client.save((err,client) => {
                chai.request(server)
                .get('/api/clients/' + client.id)
                .send(client)
                .end((err, res) =>{
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('serv');
                    res.body.should.have.property('_id').eql(client.id);
                done();
                });
            });
        });
    });
    
    describe('/PUT/:id client', ()=>{
        it('it should UPDATE a client given the id', (done)=>{
            let client = new Client(newClient.cliente);
            client.save((err, client)=>{
               chai.request(server)
               .put('/api/clients/' + client.id )
               .send({cliente:{cliente: "Alonso"}})
               .end((err, res) => {
                    res.should.have.status(200);                 ;
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Client successfully modified!');
                    res.body.client.should.have.property('cliente').eql("Alonso");
                    res.body.client.should.have.property('equipo').eql("boca");
                done();
               });
            });
        });
    });
    
    describe('/DELETE/:id client', () => {
       it('it should DELETE a client given the id', (done)=>{
           let client = new Client(newClient.cliente);
           client.save((err, client)=>{
              chai.request(server) 
              .delete('/api/clients/' + client.id)
              .end((err, res) => {
                 
                 res.should.have.status(200) ;
                 res.body.should.be.a('object')
                 res.body.should.have.property('message').eql('Successfully deleted');
                 res.body.result.should.have.property('ok').eql(1);
                 res.body.result.should.have.property('n').eql(1);
              done();
              });
           });
           
       });
    });
    
    
    
    
}); //describe Clients
    
    


