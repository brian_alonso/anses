const io = require('socket.io-client')
const bluebird = require('bluebird')
const muffler = require('/app/muffler/muffler')
const retriever = require("/app/retriever/getAlarmsFromU2k")
const fs = require('fs')

const socket = io.connect('http://backend:3001/', (err,status)=> console.log(status));


let heartBeat = (seq)=>{
 
            return new bluebird((resolve, reject)=>{
                try{
                    socket.emit('heart_beat', {msg: "heartBeat from engine", seq} , function(confirmation){
                        resolve()
                    })
                } catch(e){
                    reject(e)
                } 
            })
    
}
 
 
let u2kDown = (seq)=>{
 
            return new bluebird((resolve, reject)=>{
                try{
                    socket.emit('dispatch_action', {type: "CURRENT_ALARMS/GET", payload:{seq}} , function(confirmation){
                        resolve()
                    })
                } catch(e){
                    reject(e)
                } 
            })
    
}
 
 
  
function saveSeq(fileName="seq.json",seq){
    return new bluebird((resolve, reject)=>{
        fs.writeFile( fileName, JSON.stringify( seq ), "utf8", function(err){
            if(err)
                reject(err)
            resolve()
        } );    
    })
}

function restoreSeq(fileName="seq.json"){
    return new bluebird((resolve,reject)=>{
        fs.readFile(fileName, 'utf8', function (err, data) {
            let seq
            if (err){
              seq={seq: 1}
            }else{
                seq = JSON.parse(data)    
            } 
            
            resolve(seq)
        })
    })
}

bluebird.coroutine(function *(){
    let lastSeq = yield restoreSeq("/app/db/seq.json")
    let seq = lastSeq.seq +1
    try{
    console.log("Getting current alarms!")
    muffler.config({
        insertCallback: (id,record)=>{
            return new bluebird((resolve, reject)=>{
                try{
                    socket.emit('dispatch_action', {type: "CURRENT_ALARMS/INSERT", payload: { id, record, seq}}, function(confirmation){
                        seq = seq + 1
                        resolve()
                    })
                } catch(e){
                    reject(e)
                } 
            })
        },
        deleteCallback: (id)=>{
            return new bluebird((resolve, reject)=>{
                try{
                    socket.emit('dispatch_action', {type: "CURRENT_ALARMS/REMOVE", payload: { id,seq}}, function(confirmation){
                        seq = seq + 1
                        resolve()
                    })
                } catch(e){
                    reject(e)
                }
            })
        }
    })
    yield muffler.restoreBox("/app/db/alarmsDb.json")
    console.log("Connecting to U2k, taking currentAlarms")
    let alarms = yield retriever.getAlarms() 
    console.log("Processing alarms..")
    yield muffler.insertRecordsAsync(alarms)
    yield muffler.saveBox("/app/db/alarmsDb.json")
    yield muffler.saveData("/app/shared/currentAlarms.json")
    yield saveSeq("/app/db/seq.json", { seq })
    }catch(e){
        console.log("Hubo un error")
        muffler.resetBox()
        yield muffler.saveData("/app/shared/currentAlarms.json", {error: "Problema de conexión con U2000"})
        yield muffler.saveBox("/app/db/alarmsDb.json")
        console.log("Notificando clientes")
        yield u2kDown(seq)
        console.log("Error: ", e)
    }finally{
        console.log("Sending heartBeat")
        yield heartBeat(seq)
        console.log(seq)
        process.exit()  
    }
})()

