var request = require('request')
var xml2js = require('xml2js').parseString
var bluebird = require("bluebird");

function addZero(number){
    if(number < 10){
        return '0' + number
    } else
    return number
}

function formatDate(date) {
    //2018-10-10T18:21:59.000Z
  
  var day = addZero(date.getUTCDate())
  var month = addZero(date.getUTCMonth() + 1) 
  var year = addZero(date.getUTCFullYear())
  var hour = addZero(date.getUTCHours())
  var minutes = addZero(date.getUTCMinutes())
  var seconds = addZero(date.getUTCSeconds())
  
  return `${year}-${month}-${day}T${hour}:${minutes}:${seconds}.000Z`
}

let from = new Date()
from.setMonth(from.getMonth() -1)

from = formatDate(from)
let to = new Date()
to = formatDate(to)


var myXMLText = `
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.tmforum.org/mtop/fmw/xsd/hdr/v1" xmlns:v11="http://www.tmforum.org/mtop/rtm/xsd/ar/v1" xmlns:v12="http://www.tmforum.org/mtop/fmw/xsd/nam/v1" xmlns:v13="http://www.tmforum.org/mtop/nra/xsd/prc/v1" xmlns:v14="http://www.tmforum.org/mtop/nra/xsd/com/v1">
   <soapenv:Header>
      <v1:header>
         <v1:activityName>getHistoryAlarms</v1:activityName>
         <v1:msgName>getHistoryAlarmsRequest</v1:msgName>
         <v1:msgType>REQUEST</v1:msgType>
         <v1:security>admin:Arsat_Refefo2016</v1:security>
         <v1:communicationPattern>BulkResponse</v1:communicationPattern>
         <v1:communicationStyle>MSG</v1:communicationStyle>
         <v1:requestedBatchSize>1000000</v1:requestedBatchSize>
         <v1:fileLocationURI>sftp://ftpuser:solaris@192.168.81.245/home/sftpuser/history</v1:fileLocationURI>
         <v1:compressionType extension="?">NO_COMPRESSION</v1:compressionType>
         <v1:packingType extension="?">NO_PACKING</v1:packingType>
         
      </v1:header>
   </soapenv:Header>
   <soapenv:Body>
      <v11:getHistoryAlarmsRequest>
        <v11:filter>
            <v11:scope>
    			<v12:name>
                   <v12:rdn>
                   <v12:type>MD</v12:type>
                     <v12:value>Huawei/U2000</v12:value>
                   </v12:rdn>
                  <v12:rdn>
                  	 <v12:type>ME</v12:type>
                     <v12:value>BBNV-1800-22</v12:value>
                  </v12:rdn>
               </v12:name>
               <v12:name>
                   <v12:rdn>
                   <v12:type>MD</v12:type>
                     <v12:value>Huawei/U2000</v12:value>
                   </v12:rdn>
                  <v12:rdn>
                  	 <v12:type>ME</v12:type>
                     <v12:value>CANS-1800-11</v12:value>
                  </v12:rdn>
               </v12:name>
            </v11:scope>
            
            
            <v11:startTime>${from}</v11:startTime>
         
            <v11:endTime>${to}</v11:endTime>
         </v11:filter>
      </v11:getHistoryAlarmsRequest>
   </soapenv:Body>
</soapenv:Envelope>`









function getHistoryAlarms(){
    return new bluebird((resolve,reject) => {
        
        return request.post(
        {
            url:"http://192.168.48.82:9997/AlarmRetrieval",
            method: "POST",
            body: myXMLText,
            headers: {
                "content-type": "application/xml"
            }
        }, 
        function(err, res, body){
            if(err)
                reject(err)
            xml2js(res.body, function(err,result){
                if(err)
                    reject(err)
                if(result['soap:Envelope']["soap:Body"][0]["soap:Fault"])
                    reject(result['soap:Envelope']["soap:Body"][0]["soap:Fault"][0]["faultstring"][0])
                if(result['soap:Envelope']["soap:Body"][0]["ns3:getHistoryAlarmsResponse"])
                    resolve()     
            })//xlm
        })//function
    })
}



let getAlarms =bluebird.coroutine(function *(){
   try {
        console.log("Getting historic alarms...")
        yield getHistoryAlarms()
    } catch (e){
        throw(e)
    }
})

getAlarms()

module.exports = { getAlarms } 