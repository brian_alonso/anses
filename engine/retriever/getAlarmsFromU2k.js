var request = require('request')
var xml2js = require('xml2js').parseString
var Promise = require("bluebird");

var myXMLText = `
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.tmforum.org/mtop/fmw/xsd/hdr/v1" xmlns:v11="http://www.tmforum.org/mtop/rtm/xsd/ar/v1" xmlns:v12="http://www.tmforum.org/mtop/fmw/xsd/nam/v1" xmlns:v13="http://www.tmforum.org/mtop/nra/xsd/com/v1" xmlns:v14="http://www.tmforum.org/mtop/nra/xsd/prc/v1">
<soapenv:Header>
  <v1:header>
   <v1:activityName>getActiveAlarms</v1:activityName>
        <v1:msgName>getActiveAlarmsRequest</v1:msgName>
        <v1:msgType>REQUEST</v1:msgType>
        <v1:security>admin:Arsat_Refefo2016</v1:security>
        <v1:communicationPattern>SimpleResponse</v1:communicationPattern>
        <v1:communicationStyle>RPC</v1:communicationStyle>
  </v1:header>
</soapenv:Header>
<soapenv:Body>
  <v11:getActiveAlarmsRequest>
     <v11:filter>
        <v11:source></v11:source>


        <v11:scope>
           <v12:name>
               <v12:rdn>
               <v12:type>MD</v12:type>
                 <v12:value>Huawei/U2000</v12:value>
               </v12:rdn>
              <v12:rdn>
                 <v12:type>ME</v12:type>
                 <v12:value>BBNV-1800-22</v12:value>
              </v12:rdn>
           </v12:name>
           <v12:name>
               <v12:rdn>
               <v12:type>MD</v12:type>
                 <v12:value>Huawei/U2000</v12:value>
               </v12:rdn>
              <v12:rdn>
                 <v12:type>ME</v12:type>
                 <v12:value>CANS-1800-11</v12:value>
              </v12:rdn>
           </v12:name>
        </v11:scope>

     </v11:filter>

      </v11:getActiveAlarmsRequest>
   </soapenv:Body>
</soapenv:Envelope>
`

//console.log(myXMLText)

function deconstructor(attr){
    let strfied = JSON.stringify(attr)
    let info = attr[Object.keys(attr)[0]]
        return info.reduce((newObj,el)=>{
            let keys = Object.keys(el)
            newObj[el[keys[0]]] = el[keys[1]][0]
            return newObj
        },{})
}


let superParser = {
    "X733_SpecificProblems": (attr)=>{
        return attr['ns5:specificProblem'][0]
    },
    "X733_AdditionalInformation": deconstructor,
    "objectName": deconstructor,
    "aliasNameList": deconstructor
}



function getAlarms(){
    return new Promise((resolve, reject) => {
        try{
            return request.post(
            {
                url:"http://192.168.48.82:9997/AlarmRetrieval",
                method: "POST",
                body: myXMLText,
                headers: {
                    "content-type": "application/xml"
                },
                timeout: 3000
            }, 
            function(err, res, body){
                if(err)
                    reject(err)
                try{
                xml2js(res.body, function(err,result){
                    if(err)
                        reject(err)
                    let alarms = result['soap:Envelope']["soap:Body"][0]["ns3:getActiveAlarmsResponse"][0]["ns9:alarm"]
                    let alarmsHash = alarms.reduce( (normAlarms,alarm)=>{
                    let parsedAlarm =  Object.keys(alarm)
                                        .reduce((obj, key)=>{
                                            let value = alarm[key][0]
                                            key = key.slice(4)
                                            obj[key]=value
                                            return obj
                                        },{})
                        normAlarms[parsedAlarm["notificationId"]]=parsedAlarm
                        return normAlarms
                    },{}) //alarmsHashReduce
        
                    let finalAlarms = Object.keys(alarmsHash)
                        .reduce((finalAlarms,key)=>{
                            let currentAlarm = alarmsHash[key]
                            let stringAlarms = Object.keys(currentAlarm)
                                .reduce((stringAlarm, attr)=>{
                                    
                                    if(typeof alarmsHash[key][attr] === "string"){
                                        stringAlarm[attr] = alarmsHash[key][attr]
                                    } else {
                                   
                                        if(superParser[attr]){
                                                stringAlarm[attr] = superParser[attr](alarmsHash[key][attr])
                                        }                                }
                                    return stringAlarm
                                },{})
                             finalAlarms[currentAlarm.notificationId] = stringAlarms
                                return finalAlarms
                        },{})
                        resolve( finalAlarms)
                })}catch(e){
                    reject()
                }//xlm
            })//function
        }catch(e){
            reject(e)
        }
    })
    
}

module.exports = { getAlarms } 