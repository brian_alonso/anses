const io = require('socket.io-client')
const bluebird = require('bluebird')
const muffler = require('/app/muffler/muffler')
const retriever = require("/app/retriever/getAlarmsFromU2k")
const fs = require('fs')

const socket = io.connect('http://backend:3001/', (err,status)=> console.log(status));


let heartBeat = (seq)=>{
 
            return new bluebird((resolve, reject)=>{
                try{
                    socket.emit('heart_beat', {msg: "heartBeat from engine", seq} , function(confirmation){
                        resolve()
                    })
                } catch(e){
                    reject(e)
                } 
            })
    
}
 
 
 
  
function saveSeq(fileName="seq.json",seq){
    return new bluebird((resolve, reject)=>{
        fs.writeFile( fileName, JSON.stringify( seq ), "utf8", function(err){
            if(err)
                reject(err)
            resolve()
        } );    
    })
}

function restoreSeq(fileName="seq.json"){
    return new bluebird((resolve,reject)=>{
        fs.readFile(fileName, 'utf8', function (err, data) {
            let seq
            if (err){
              seq={seq: 1}
            }else{
                seq = JSON.parse(data)    
            } 
            
            resolve(seq)
        })
    })
}

bluebird.coroutine(function *(){
    console.log("Getting current alarms!")
    let lastSeq = yield restoreSeq("/app/db/seq.json")
    let seq = lastSeq.seq +1
    muffler.config({
        insertCallback: (id,record)=>{
            return new bluebird((resolve, reject)=>{
                try{
                    socket.emit('dispatch_action', {type: "CURRENT_ALARMS/INSERT", payload: { id, record, seq}}, function(confirmation){
                        seq = seq + 1
                        resolve()
                    })
                } catch(e){
                    reject(e)
                } 
            })
        },
        deleteCallback: (id)=>{
            return new bluebird((resolve, reject)=>{
                try{
                    socket.emit('dispatch_action', {type: "CURRENT_ALARMS/REMOVE", payload: { id,seq}}, function(confirmation){
                        seq = seq + 1
                        resolve()
                    })
                } catch(e){
                    reject(e)
                }
            })
        }
    })
    yield muffler.restoreBox("/app/db/alarmsDb.json")
    let alarms = yield retriever.getAlarms()    
    yield muffler.insertRecordsAsync(alarms)
    yield muffler.saveBox("/app/db/alarmsDb.json")
    yield muffler.saveData("/app/shared/currentAlarms.json")
    yield saveSeq("/app/db/seq.json", { seq })
    yield heartBeat(seq)
    console.log(seq)
    process.exit()  
})()






/*





socket.on('connect_failed', function(){
    console.log('Connection Failed');
});

socket.on('connect', function(){
    console.log('Connected!');
});

socket.on('connect_timeout', function(){
    console.log('Timeout');
});

socket.emit('heart_beat', {"status": "ok"} , function(ok){
                              console.log("Socket emit ok");
                              process.exit();
            });
*/