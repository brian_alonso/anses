const io = require('socket.io-client')
const bluebird = require('bluebird')
//const muffler = require('/app/muffler/muffler')
//const retriever = require("/app/retriever/getAlarmsFromU2k")
const fs = require('fs')

const socket = io.connect('http://backend:3001/', (err,status)=> console.log(status));

  

 let insert = (id, record, seq)=>{
 
            return new bluebird((resolve, reject)=>{
                try{
                    socket.emit('dispatch_action', {type: "CURRENT_ALARMS/INSERT", payload: { id, record, seq}}, function(confirmation){
                  //      seq = seq + 1
                        resolve()
                    })
                } catch(e){
                    reject(e)
                } 
            })
 }
 let remove = (id, record, seq)=>{
        
        new bluebird((resolve, reject)=>{
                try{
                    socket.emit('dispatch_action', {type: "CURRENT_ALARMS/REMOVE", payload: { id,seq}}, function(confirmation){
                   //     seq = seq + 1
                        resolve()
                    })
                } catch(e){
                    reject(e)
                }
            })
        
}

let sleep = (time)=> new bluebird(res => setTimeout(()=>{ res() },time*1000))

function createAlarm(source, location, description){
    return ({
                "objectName":{ "ME": source}, 
                "nativeProbableCause": description, 
                "X733_AdditionalInformation":{ 
                    "LocationInfo": location
                }
            }
    )
}



bluebird.coroutine(function *(){
    yield insert(   "failViaCABA1CANSrx", createAlarm("CANS-1800-11","0-1-A-5-F2OBU-1(IN)-OTS:1","MUT_LOS"),1)
    yield sleep(7)
    yield insert(   "failViaCABA1BBNVrx", createAlarm("BBNV-1800-22","2-1-B-6-OLPN-1(RI1/TO1)-CLIENT:1","R_LOS"),12)
    yield sleep(7)
    yield insert(   "failViaCABA2CANSrx", createAlarm("BBNV-1800-22","2-1-B-5-F2OBU-1(IN)-OTS:1","MUT_LOS"),18)
    yield sleep(7)
    yield insert(   "failViaCABA2BBNVrx", createAlarm("CANS-1800-11","2-1-B-6-OLPN-2(RI2/TO2)-CLIENT:1","R_LOS"),20)
  
})()

