const muffler = deps =>
{
  
 //dependencies
 
  
  const boxFile = "box.json"
  const fs = deps.fs
  const bluebird = deps.bluebird
  
  let box = {}
  
  let config = {
      insertCallback: (id,record)=>{console.log(`Inserting record: ${record}`)},
      deleteCallback: (id)=>{console.log(`Deleting record: ${id}`)}
  }
  
  
  function setConfig(objConfig){
    for(var prop in objConfig){
        config[prop]=objConfig[prop];
    }
    return publicObject
  }
  
  
    
        
//   function config(cfgi={
//       io: null,
//       backendLocation: "http://backend:3001",
//       remoteInsertAction: "REMOTE/INSERT_ALARM",
//       remoteDeleteAction: "REMOTE/REMOVE_ALARM",
//       heartBeatChannel: "REMOTE/HEART_BEAT",
//       remoteChannel: "engineBroadcast",
//   }){
//       cfg = cfgi
//   }
  
  function resetBox(){
      box = {}
  }
  
  function getData(){
      return Object.keys(box)
        .reduce((strippedData, key)=>{
            
            strippedData[key] = box[key].record
            return strippedData
        },{})
  }
  
  
  function saveBox(fileName="box.json"){
    return new bluebird((resolve, reject)=>{
        fs.writeFile( fileName, JSON.stringify( box ), "utf8", function(err){
            if(err)
                reject(err)
            resolve()
        } );    
    })
  }
  
 function saveData(fileName="alarms.json", errorMsg=false){
    return new bluebird((resolve, reject)=>{
        let dataToBeSaved = errorMsg ? errorMsg : getData()
        fs.writeFile( fileName, JSON.stringify( dataToBeSaved ), "utf8", function(err){
            if(err)
                reject(err)
            resolve()
        } );    
    })
  }
  
  function restoreBox(fileName="box.json"){
      return new bluebird((resolve,reject)=>{
        fs.readFile(fileName, 'utf8', function (err, data) {
        if (err){
          reject(err)  
        } 
            box = JSON.parse(data)
            resolve()
        })
    })
  }
  
  function getBox(){
      return box
  }
  
  function insertRecord(id, record){
    if(!box[id]){
        box[id] = {
            id,
            record,
            alive: true
        }
        if(typeof config.insertCallback === "function"){
            config.insertCallback(id, record)    
        }
        
    }else{
        box[id].alive = true
    }
    
  }
  
  
    function insertRecordAsync(id, record){
        if(!box[id]){
            box[id] = { id, record, alive: true}
            if(typeof config.insertCallback === "function"){
                return config.insertCallback(id, record)    
            }
        }else{
            box[id].alive = true
        }
        return new bluebird.resolve()
    }
  
  
  function kill(){
      box = Object.keys(box)
        .reduce((newBox, key)=>{
            box[key].alive = false
            newBox[key] = {...box[key] }
            return newBox
        },{})
  }
  
  function removeDead(){
      box = Object.keys(box)
        .reduce((newBox, key)=>{
            if(box[key].alive){
                newBox[key] = box[key]
            }else{
                if(typeof config.deleteCallback === "function") {
                   config.deleteCallback(key)
                }
            }
            return newBox
        },{})
  }
  
  function insertRecords(records){
      kill()
      Object.keys(records).forEach( key => {
     //     console.log(records)
          insertRecord(key,records[key])
      })
      removeDead()
  }
  
  function insertRecordsAsync(records){
    kill()
    let recordIds = Object.keys(records)
    return bluebird.each(recordIds, function(recordId) {
        return insertRecordAsync(recordId, records[recordId])
    }).then(()=>{
        removeDead()
        return
    })
    
  }
  
   let publicObject = {
       config: setConfig,
       getBox,
       resetBox,
       saveBox,
       restoreBox,
       insertRecords,
       getData,
       saveData,
       insertRecordsAsync
   }

 

        return publicObject

}





module.exports = muffler



/*

    pide alarmas al u2000

    muffler.

    Se pone muffler flag en cero.

    Se piden alarmas.
    Por cada una si está en muffler, pone flago en 1
    si no está, inserta en muffler y broadcast a frontends.

    Una vez terminado, se verifica en el muffler, que registro no está marcado.

    Se lo borra del muffler y se envía broadcast a frontends.

    tareas:

    armar entorno sin ftp, levantar ftp local, activar wifi.

    el muffler es el nuevo engine, pero corre en el mismo docker container por simplicidad.

    el backend consulta al muffler por el listado de todas las alarmas.

    el muffler se conecta a db????
*/