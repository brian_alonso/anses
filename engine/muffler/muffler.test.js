let chai = require('chai');
let should = chai.should();
let mockups = require('./mockups.js')
const muffler = require('./muffler')
const fs = require('fs')


describe('atomicServices', () => {
    beforeEach((done) => {
    done();
    });
    after((done)=>{
     fs.unlink("test.json")
     done()
    });
  

    describe('Insert Records in empty box', () => {
        it('saves all the records in the box', () => {
            muffler.insertRecords(mockups.fourRecords)
            let box = muffler.getBox()
            box.should.to.include.all.keys('1','2','3','4')
        })
        it('delete record if not present', () => {
            muffler.insertRecords(mockups.fourRecords)
            let box = muffler.getBox()
            box.should.to.include.all.keys('1','2','3','4')
            muffler.insertRecords(mockups.threeRecords)
            box.should.to.include.all.keys('1','2','3')
            box = muffler.getBox()
            box.should.to.not.include.any.keys('4')
        })
        it('delete all records', () => {
            muffler.insertRecords(mockups.fourRecords)
            let box = muffler.getBox()
            box.should.to.include.all.keys('1','2','3','4')
            muffler.insertRecords({})
            box = muffler.getBox()
            box.should.to.not.include.any.keys('1','2','3','4')
        })
        it('keep all records', () => {
            muffler.insertRecords(mockups.fourRecords)
            let box = muffler.getBox()
            box.should.to.include.all.keys('1','2','3','4')
            muffler.insertRecords(mockups.fourRecords)
            box = muffler.getBox()
            box.should.to.include.all.keys('1','2','3','4')
        })
        it('add one record and delete another one', () => {
            muffler.insertRecords(mockups.fourRecords)
            let box = muffler.getBox()
            box.should.to.include.all.keys('1','2','3','4')
            muffler.insertRecords(mockups.fourRecords2Diff)
            box = muffler.getBox()
            box.should.to.include.all.keys('1','2','5','6')
            box.should.to.not.include.any.keys('3','4')
        })
        it("saves and restore box correctly", ()=>{
            muffler.insertRecords(mockups.fourRecords)
            let box = muffler.getBox()
            box.should.to.include.all.keys('1','2','3','4')
            muffler.saveBox("test.json").then(()=>{
                muffler.resetBox()
                box = muffler.getBox()
                box.should.to.not.include.any.keys('1','2','3','4')
                return muffler.restoreBox("test.json")
            })
            .then(()=>{
                box = muffler.getBox()
                box.should.to.include.all.keys('1','2','3','4')    
            })
            
            
        })
            
        
        
    }) //describe crossInformation

    describe('otherThins', () => {
        it('Should be true', () => {
        //   let result = atomicServices.create(mockups.l2lOnlyVirt2Peers)
        //   result[0].notas[0].should.equal("Servicio configurado en ambos extremos con mismo policy en virtuales")
        //   result[0].totalBw.should.equal(1000000)
        })
        
    })
})
