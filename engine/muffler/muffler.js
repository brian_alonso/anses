/**
 * Creates an atomic services which will be analized.
 * It tells you what to do in the network???
 * @module atomicServices
 */
//dependecy


const fs = require("fs");
const bluebird = require("bluebird");

const deps = { 
        fs,
        bluebird
}

const factory = require('./muffler.factory')

module.exports = factory(deps)
