import React from 'react';
import ReactDOM from 'react-dom';
import './socket/socket.js'




//router

import { BrowserRouter } from 'react-router-dom'

//redux provider

import {Provider} from 'react-redux'


import { MainView } from './_views/MainView'




import { store } from './_store'




ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <MainView  />
    </BrowserRouter>
  </Provider>
    ,document.getElementById('root'));


