import { fork } from 'redux-saga/effects'
import _ from 'lodash'

//import * as propiedadSagas from '../example/sagas'

import * as CurrentAlarmsSagas from '../CurrentAlarms/sagas'
import * as HistoryAlarmsSagas from '../HistoryAlarms/sagas'


let allCurrentAlarmsSagas = _.map(CurrentAlarmsSagas, (el=> fork(el)))
let allHistoryAlarmsSagas = _.map(HistoryAlarmsSagas, (el=> fork(el)))

function* root(){
    yield [
            allCurrentAlarmsSagas,
            allHistoryAlarmsSagas
        ]
}

let allSagas = root

export { allSagas }
