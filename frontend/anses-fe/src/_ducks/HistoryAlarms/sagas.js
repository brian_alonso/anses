import { takeEvery, put} from 'redux-saga/effects'
//import { takeEvery, put, take, call, race, select} from 'redux-saga/effects'



function* getSaga(action){
    try{
        yield put({type: 'HISTORY_ALARMS/LOADING'})
        let response = yield fetch("/api/historyAlarms")
        if(response.ok){
            let parsedResponse = yield response.json()
            yield put({type: 'HISTORY_ALARMS/LOAD', payload: parsedResponse})
            yield put({type: "HISTORY_ALARMS/LOAD_OK"})    
        }else{
            yield put({type: "HISTORY_ALARMS/LOAD_FAIL"})    
        }
   }catch(e){
       yield put({type: "HISTORY_ALARMS/LOAD_FAIL"})    
   }
    
}


export function* watchCrearPeriodosRecargos(){
    yield takeEvery('HISTORY_ALARMS/GET', getSaga)
}
