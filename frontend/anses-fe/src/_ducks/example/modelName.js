let reducerExample = function(state={}, action) {
  switch(action.type){
    case "ACTION_1":
          
          return { action: action.payload}
    case "ACTION_2":
        return { action: 2}
    default: return state;
  }

}


function simpleAction(){
    return {type: 'ACTION_1', payload: " "}
}

function callSaga(){
  return { type: 'SAGA'}
}


let actions = {
 simpleAction,
 callSaga
}



export { reducerExample, actions }