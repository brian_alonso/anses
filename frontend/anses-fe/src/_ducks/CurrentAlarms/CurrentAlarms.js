import Immutable, {Map} from 'immutable';
import { combineReducers } from 'redux'

let alarms= function(state=Map(), action) {

    switch(action.type){
        case "CURRENT_ALARMS/LOAD":
            return Immutable.fromJS(action.payload);
     
        case "CURRENT_ALARMS/INSERT":
            return state.set(action.payload.id, Immutable.fromJS(action.payload.record))
     
        case "CURRENT_ALARMS/REMOVE":
            return state.delete(action.payload.id)   
    
        default: return state;
  }  
}

let request= function(state=Map({ request: "none"}), action){
    switch(action.type){
        case "CURRENT_ALARMS/LOADING":
            return Map({ request: "loading"})
        case "CURRENT_ALARMS/LOAD_OK":
            return Map({ request: "ok"})
        case "CURRENT_ALARMS/LOAD_FAIL":
            return Map({ request: "fail"})
        default: return state;
    }
}


let CurrentAlarms = combineReducers({
    alarms,
    request, 
})


/*

{ id: "a1" record: "brian", seq: "1" }
{ id: "a2" record: "brian", seq: "1" }
{ id: "a3" record: "brian", seq: "1" }
{ id: "a4" record: "brian", seq: "1" }
{ id: "a5" record: "brian", seq: "1" }


{ type: "CURRENT_ALARMS/LOAD", payload: { "c1":"pepito" , "c2": "briancito"}}
{ type: "CURRENT_ALARMS/REMOVE", payload: { id: "c1"}}
{ type: "CURRENT_ALARMS/INSERT", payload: { id: "a5", record: "recorddd", seq:"2"}}



*/


  
function getCurrentAlarms(){
    return {type: 'CURRENT_ALARMS/GET'}//handle by the saga
}




let actions = {
 getCurrentAlarms
}



export { CurrentAlarms, actions }


/*

import Immutable, {List, Map} from 'immutable';


export default function(state=List(), action) {
    
    
  
  
  switch(action.type){
    case "NOTIFICATION":
      let algo = state.filter((client, index) => client.get("checked"))
      console.log(algo)
      return state
    case "SET_CLIENTS":
      
      // return List( [
      //               Map({a:"a"}), 
      //               Map({b:"b"})
                    
      //               ]) ;
      return Immutable.fromJS(action.payload);
    case "NEW_CLIENT":
      
      return state.push(Map(action.payload));
    
    case "UPDATE_CLIENT":
      
      return state.update(
      state.findIndex(function(item) { 
          
          return item.get("_id") === action.payload._id; 
        }), function(item) {
        //  action.payload.checked = item.get("checked")
          return Map(action.payload);
        }
      );
      
    case "TOGGLE_CLIENT":
      //immutable list, update( index, (itemToBeReplaced => newItem))
      return state.update( 
                            state.findIndex( (item)=> item.get("_id") === action.payload), 
                            (item)=> item.update('checked', value => !value)
      )
    
    // case "TOGGLE_CLIENT":
    //   return state.update(
    //     state.findIndex(function(item) { 
          
    //       return item.get("_id") === action.payload; 
    //     }), function(item) {
    //     // return item.update('checked', value => !item.get("checked"));
    //         return item.update('checked', value => !value)
          
    //     }
    //   );
      
      
    
    
    case "DELETE_CLIENT":
        return state.filterNot(
          (el) => el.get("_id") === action.payload
          )
    default: return state;
  }  
      
}





*/