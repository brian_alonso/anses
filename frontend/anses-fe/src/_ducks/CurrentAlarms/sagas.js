import { takeEvery, put} from 'redux-saga/effects'
//import { takeEvery, put, take, call, race, select} from 'redux-saga/effects'



function* getSaga(action){
    
    try{
        yield put({type: 'CURRENT_ALARMS/LOADING'})
        let response = yield fetch("/api/currentAlarms")
        if(response.ok){
            let parsedResponse = yield response.json()
            if(parsedResponse["error"]){ throw parsedResponse["error"] }
            yield put({type: 'CURRENT_ALARMS/LOAD', payload: parsedResponse})
            yield put({type: "CURRENT_ALARMS/LOAD_OK"})    
        }else{
            yield put({type: "CURRENT_ALARMS/LOAD_FAIL"})    
        }
   }catch(e){
       yield put({type: "CURRENT_ALARMS/LOAD_FAIL"})    
   }
    
}


export function* watchCrearPeriodosRecargos(){
    yield takeEvery('CURRENT_ALARMS/GET', getSaga)
}
