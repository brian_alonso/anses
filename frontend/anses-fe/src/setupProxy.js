const proxy = require('http-proxy-middleware');

module.exports = function(app) {
 app.use(proxy('/api', { target: 'http://backend:3001/' }));
  
  // let wsProxy = proxy({
  //   target: "http://backend:3000",
  //   ws: true
  // })
 // app.use(proxy('/ws', {target: 'http://backend:3001/ws', ws:true} ));
};