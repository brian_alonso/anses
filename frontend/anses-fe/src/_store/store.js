import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'
import { allSagas } from '../_ducks/_sagas'
import allReducers from '../_reducers';




const sagaMiddleware = createSagaMiddleware()


export const store = createStore(
  allReducers,
  composeWithDevTools(
    applyMiddleware(
        sagaMiddleware
    )
  )
);


sagaMiddleware.run(allSagas)
