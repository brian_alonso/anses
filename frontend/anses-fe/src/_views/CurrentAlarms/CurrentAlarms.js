import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Topology } from '../Topology'
import { CurrentAlarmsTable } from '../CurrentAlarmsTable'
//import { bindActionCreators } from 'redux'
//import { actions  } from '../../_ducks/example'
//import {  Route, Switch } from 'react-router-dom'


class CurrentAlarms extends Component {
  render() {
    return (
    <div>
         <section className="section">
            <Topology />
            <CurrentAlarmsTable />

         </section>
      
     
     </div>
    );
  }
}


function mapStateToProps(state){
  return {
   //example: state.,
    
  }
}

function matchDispatchToProps(dispatch){
  return {
    actions:{
 //     actionSaga: bindActionCreators(actions, dispatch),
    }
  }
}


CurrentAlarms = connect(mapStateToProps, matchDispatchToProps)(CurrentAlarms)

export { CurrentAlarms };
