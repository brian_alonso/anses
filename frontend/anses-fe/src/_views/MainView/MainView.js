import React, { Component } from 'react';
import { Header } from '../Header'
import { CurrentAlarms } from '../CurrentAlarms'
import { HistoryAlarms } from '../HistoryAlarms'
import {  Route, Switch } from 'react-router-dom'
import { library } from '@fortawesome/fontawesome-svg-core'

import { fas } from '@fortawesome/free-solid-svg-icons'
import '../../sass/main.scss';

library.add( fas)







class MainView extends Component {
  render() {
    return (
    <div>
     <Header />
     <main>
      <Switch>
        <Route exact path="/" component={CurrentAlarms} />
        <Route exact path="/historyAlarms" component={HistoryAlarms} />
     </Switch>   
     </main>
     
     
     
     </div>
       
       
    
       
           
       
    );
  }
}


export { MainView };
