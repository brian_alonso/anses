import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'
import logoWhite from '../../img/arsat.png'




class Header extends Component {
  render() {
    return (
     <header className="header">
       <div className="header__logo-box">
         <img src={logoWhite} alt="Logo" className="header__logo" />
      </div>
      <div className="header__text-box">
         <h1 className="heading-primary">
           <span className="heading-primary--sub">Enlace Anses</span>
         </h1>
      </div>
      <div className="header__keypad">
        <NavLink exact to="/"  className="btn btn--white btn--animated" activeClassName="btn--selected">Current Alarms</NavLink>
        <NavLink exact to="/historyAlarms" className="btn btn--white btn--animated" activeClassName="btn--selected">History Alarms</NavLink>
      </div>
       
     </header>

    );
  }
}





export { Header };
