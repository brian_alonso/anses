import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions  } from '../../_ducks/CurrentAlarms'
import { bindActionCreators } from 'redux'

//import { actions  } from '../../_ducks/example'
//import {  Route, Switch } from 'react-router-dom'

const severities = {
    NONE: 0,
    WARNING: 1,
    MINOR: 2,
    MAJOR: 3,
    CRITICAL: 4
  }
const severitiesArray = ["NONE","WARNING","MINOR", "MAJOR", "CRITICAL"]

const severitiesReducer = (bigSeverity, alarm) => {
    let severity = severities[alarm.get("perceivedSeverity")]
    
      return severity > bigSeverity ? severity : bigSeverity
}

function detectAlarm(alarmsMap,source,location,description){
  let lastKey= false
  let filtered = alarmsMap.filter( (al,key)=> {
    if(key==="brian" || lastKey){
      console.log(JSON.stringify(al.toJS(), null, " "))
      if(lastKey === false){
        lastKey= true
      }else{
        lastKey= false
      }
        
    }
    return al.getIn(["objectName","ME"])===source && al.get("nativeProbableCause")===description && al.getIn(["X733_AdditionalInformation","LocationInfo"]) === location
  })
  return filtered.count() > 0
  //return alarmsMap.filter( al=> al.getIn(["objectName","ME"])===source && al.get("nativeProbableCause")===description && al.getIn(["X733_AdditionalInformation","LocationInfo"]) === location ).count() > 0  
}


class Topology extends Component {
  
  render() {
    
     if(this.props.request==="fail")
      return null
    
    if(this.props.request==="loading" || this.props.request==="none")
      return null
    
    
    if(this.props.request==="ok"){
      //console.log(detectAlarm(this.props.currentAlarms, "CANS-1800-11","0-1-A-3-F2LTX-8(RX6/TX6)-CLIENT:1","REM_SF"))
      
      /*
      simulate alarma
       {type: "CURRENT_ALARMS/INSERT",payload: { id:"brian", record:{"sourceTime": "2019-10-27T16:53:28.000Z", "objectName":{ "ME": "CANS-1800-11"}, "nativeProbableCause":"MUT_LOS", "X733_AdditionalInformation":{ "LocationInfo": "0-1-A-5-F2OBU-1(IN)-OTS:1"}}}} 
      {objectName:{ ME: "CANS-1800"}, nativeProbableCause:{"MUT_LOS"}, X733_AdditionalInformation:{ LocationInfo: "0-1-A-5-F2OBU-1(IN)-OTS:1"}}
      */

      let failViaCABA1CANSrx = detectAlarm(this.props.currentAlarms, "CANS-1800-11","0-1-A-5-F2OBU-1(IN)-OTS:1","MUT_LOS")
      let failViaCABA1BBNVrx = detectAlarm(this.props.currentAlarms, "BBNV-1800-22","2-1-B-6-OLPN-1(RI1/TO1)-CLIENT:1","R_LOS") ||
                               detectAlarm(this.props.currentAlarms, "BBNV-1800-22","0-1-A-6-OLPN-1(RI1/TO1)-CLIENT:1","R_LOS")
      
      let failViaCABA2CANSrx= detectAlarm(this.props.currentAlarms, "BBNV-1800-22","2-1-B-5-F2OBU-1(IN)-OTS:1","MUT_LOS")
      let failViaCABA2BBNVrx= detectAlarm(this.props.currentAlarms, "CANS-1800-11","2-1-B-6-OLPN-2(RI2/TO2)-CLIENT:1","R_LOS") ||
                              detectAlarm(this.props.currentAlarms, "CANS-1800-11","0-1-A-6-OLPN-2(RI2/TO2)-CLIENT:1","R_LOS")


      let biggerSeverityCANS = this.props.currentAlarms.filter(alarm => alarm.getIn(["objectName","ME"])==="CANS-1800-11").reduce(severitiesReducer,0)
      let biggerSeverityBBNV = this.props.currentAlarms.filter(alarm => alarm.getIn(["objectName","ME"])==="BBNV-1800-22").reduce(severitiesReducer,0)
                               this.props.currentAlarms.filter(alarm => alarm.getIn(["objectName","ME"])==="BBNV-1800-22")
    
    
    
      return (
    
              <div className="box">
                <div className={"comp comp--left comp--alarm comp--alarm--"+ severitiesArray[biggerSeverityCANS]}>
                  <span className="comp__text">CANS-1800-11</span>
                </div>
                <div className="loader loader--left loader--critical"></div>
                <div className="id-enlace id-enlace--top">via CABA 1</div>
                <div className={"con con--1 "+( failViaCABA1BBNVrx ? "con--fail" : "") }></div>
                <div className={"byte byte--1 "  +( failViaCABA1BBNVrx ? "byte--fail" : "")}></div>
                <div className={"con con--2 "+( failViaCABA1CANSrx ? "con--fail" : "")}></div>
                <div className={"byte byte--2 "  +( failViaCABA1CANSrx ? "byte--fail" : "")}></div>
                <div className="id-enlace id-enlace--bottom">via CABA 2</div>
                <div className={"con con--3 "+( failViaCABA2BBNVrx ? "con--fail" : "")}></div>
                <div className={"byte byte--3 "  +( failViaCABA2BBNVrx ? "byte--fail" : "")}></div>
                <div className={"con con--4 "+( failViaCABA2CANSrx ? "con--fail" : "" )}></div>
                <div className={"byte byte--4 "  +( failViaCABA2CANSrx ? "byte--fail" : "")}></div>
                <div className={"comp comp--right comp--alarm comp--alarm--" + severitiesArray[biggerSeverityBBNV]}>
                  <span className="comp__text">BBNV-1800-22</span>
                </div>
                <div className="loader loader--right"></div>
              </div>
      
      );
    }
  }
}

function mapStateToProps(state){
  return {
   currentAlarms: state.CurrentAlarms.alarms,
   request: state.CurrentAlarms.request.get("request")
  }
}

function matchDispatchToProps(dispatch){
  return {
    actions:{
      currentAlarms: bindActionCreators(actions, dispatch),
      
    }
  }
}


Topology = connect(mapStateToProps, matchDispatchToProps)(Topology)

export { Topology };
