import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { actions  } from '../../_ducks/HistoryAlarms'
import moment from 'moment'
import { Loader } from '../../_components/_loader'
import { ConnectionFail } from '../../_components/_connectionFail'

//import {  Route, Switch } from 'react-router-dom'

let esLocale = require('moment/locale/es')
moment.locale('es', esLocale)

class HistoryAlarmsTable extends Component {
  
  
  constructor(props){
    super(props)
    this.tbodyRef = React.createRef()
    this.theadRef = React.createRef()
  }
  
  
  componentDidMount(){
    window.addEventListener('scroll', this.handleScroll, { passive: true })
    this.props.actions.historyAlarms.getHistoryAlarms()
  }
  
  

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

 handleScroll= (event)=> {
 //   //console.log(this.myRef)
      if(this.tbodyRef.current && this.theadRef.current){
        let y = this.tbodyRef.current.getBoundingClientRect().y
      if(y < 97){
        this.theadRef.current.classList.add('fixTableHeader')
      }else{
        this.theadRef.current.classList.remove('fixTableHeader')
      }  
    }
  }
  
  
  
  render() {
    
    
    
    
    if(this.props.request==="fail")
      return(<ConnectionFail/>)
    
    if(this.props.request==="loading" || this.props.request==="none")
      return(<Loader/>)
    
    
    if(this.props.request==="ok"){
  
  
    let tds = this.props.historyAlarms.sort((a, b) => {
          if (new Date(a.get("sourceTime")) < new Date(b.get("sourceTime"))) { return 1; }
          if (new Date(a.get("sourceTime")) > new Date(b.get("sourceTime"))) { return -1; }
          if (new Date(a.get("sourceTime")) === new Date(b.get("sourceTime"))) { return 0; }
          return null
          })
      .map((el,key)=>{
            let alarm = el.toJS()
            window.alarm = alarm
          //  console.log(alarm)
            return(
              <tr key={key} className={alarm.perceivedSeverity}>
                <td className="seven">{moment(alarm.sourceTime).format("H:mm:ss D/MM/YY")}</td>
                <td className="seven">{moment(alarm.vendorExtensions.sourceEndTime).format("H:mm:ss D/MM/YY")}</td>
                <td className="two sitio">{alarm.objectName.ME}</td>
                <td className="three">{alarm.X733_AdditionalInformation.LocationInfo}</td>
                <td className="four">{alarm.additionalText}</td>
                <td className="five">{alarm.X733_AdditionalInformation.affectedSNCNativeEMSName}</td>
                <td className="six">{alarm.X733_SpecificProblems}</td>
              </tr>
            )
          })
    
  
  
  
  
    return (
        <table>
          <thead>
            <tr>
              <th className="seven">Aparición</th>
              <th className="seven">Clareo</th>
              <th className="two">Sitio</th>
              <th className="three">Componente</th>
              <th className="four">Descripción</th>
              <th className="five">Relacionado a servicio</th>
              <th className="six">Diagnóstico</th>
            </tr>
        </thead>
        <thead className="thead--hidden" ref={this.theadRef}>
            <tr>
              <th className="seven">Aparición</th>
              <th className="seven">Clareo</th>
              <th className="two">Sitio</th>
              <th className="three">Componente</th>
              <th className="four">Descripción</th>
              <th className="five">Relacionado a servicio</th>
              <th className="six">Diagnóstico</th>
            </tr>
        </thead>
        <tbody ref={this.tbodyRef}>
          {tds.valueSeq()}
        </tbody>
  
</table>
    )
    ;}//if ok
  }
}


function mapStateToProps(state){
  return {
   historyAlarms: state.HistoryAlarms.alarms,
   request: state.HistoryAlarms.request.get("request")
  }
}

function matchDispatchToProps(dispatch){
  return {
    actions:{
      historyAlarms: bindActionCreators(actions, dispatch),
    }
  }
}


HistoryAlarmsTable = connect(mapStateToProps, matchDispatchToProps)(HistoryAlarmsTable)

export { HistoryAlarmsTable };
