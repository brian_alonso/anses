import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class ConnectionFail extends Component {
  
  

  render() {
    
      return(
          <div className="loading">
            <FontAwesomeIcon icon="skull"   size="10x" color="white" />
            <div style={{fontSize: "2rem", color: "white"}}>Hubo un problema con la conexión.</div>
          </div>
          )
    
  }
}




export { ConnectionFail };
