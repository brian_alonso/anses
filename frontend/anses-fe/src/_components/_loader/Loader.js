import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class Loader extends Component {
  
  

  render() {
    
      return(<div className="loading"><FontAwesomeIcon icon="cog" spin  size="10x" color="white" /></div>)
    
  }
}




export { Loader };
