import { store } from '../_store'
import { actions  } from '../_ducks/CurrentAlarms'
import io from 'socket.io-client';
let wsAddress = 'http://127.0.0.1:3001/'

let sequenceHandlerFactory = function(){
    let seq = false
    
    return {
        compareSequence: (currentSeq)=>{
            if(!seq){
                seq = currentSeq
            }else{
                if(currentSeq === seq +1 || currentSeq === seq){
                    seq = currentSeq
                }else{
                    seq = false
                    store.dispatch(actions.getCurrentAlarms())
                }
            }
        }
    }
}

let sequenceHandler = sequenceHandlerFactory()



if(process.env.NODE_ENV === "production"){
  wsAddress= "/"
}
console.log("Sockets ok!")

var socket = io.connect(wsAddress, function(socket, err){
    console.log("Socket Conection ok")
});
  
socket.on('dispatch_action', function (action) {
   sequenceHandler.compareSequence(action.payload.seq)
   store.dispatch(action)
});



// heart_beat system



  socket.on('heart_beat', function (data) {
    sequenceHandler.compareSequence(data.seq)
    lastUpdate = new Date();
  });

var lastUpdate = false

function compareDates() {
  if(!lastUpdate) return false
  let now = new Date()
  let diff =  now - lastUpdate
  if (diff > 80000)
    store.dispatch(actions.getCurrentAlarms())
    
} 
setInterval(compareDates, 10000)








































  
 export default socket
 
 
 /*
 
 
var lastUpdate = false

var socket = io.connect('/');
  socket.on('update_situacion', function (data) {
    //console.log(data);
    store.dispatch(updateClient(data));    
    lastUpdate = new Date();
  });
  socket.on('heart_beat', function (data) {
   // console.log(data);
    lastUpdate = new Date();
    //store.dispatch(setLastUpdate());    
  });

function compareDates() {
  if(!lastUpdate) return false
  //console.log("Comparing dates");
  let now = new Date()
  let diff =  now - lastUpdate
  //console.log(diff);
  if (diff > 80000)
    store.dispatch(setInterfaceState("Down"))
  else
    store.dispatch(setInterfaceState("Up"))
} 
var id = setInterval(compareDates, 10000)


  
export default socket


*/